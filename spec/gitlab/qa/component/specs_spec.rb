describe Gitlab::QA::Component::Specs do
  let(:docker_command) { spy('docker command') }
  let(:suite) { spy('suite') }

  before do
    stub_const('Gitlab::QA::Docker::Command', docker_command)
    allow(Gitlab::QA::Runtime::Scenario.attributes).to receive(:include?).and_return(false)
  end

  around do |example|
    # In CI, QA_IMAGE could be set and modify the tests' behavior. Ensure we reset it to nil.
    ClimateControl.modify(QA_IMAGE: nil) { example.run }
  end

  describe '#perform' do
    it 'bind-mounts a docker socket' do
      described_class.perform do |specs|
        specs.suite = suite
        specs.release = spy('release', login_params: nil)
      end

      expect(docker_command).to have_received(:volume)
        .with('/var/run/docker.sock', '/var/run/docker.sock')
    end

    it 'bind-mounds volume with screenshots in an appropriate directory' do
      allow(SecureRandom).to receive(:hex).and_return('def456')
      allow(Gitlab::QA::Runtime::Env)
        .to receive(:host_artifacts_dir)
        .and_return('/tmp/gitlab-qa/gitlab-qa-run-2018-07-11-10-00-00-abc123')

      release = double('release', edition: :ce, project_name: 'gitlab-ce', qa_image: 'gitlab-ce-qa', qa_tag: 'latest')
      allow(release)
        .to receive(:login_params)
        .and_return(nil)

      described_class.perform do |specs|
        specs.suite = suite
        specs.release = release
      end

      expect(docker_command).to have_received(:volume)
        .with('/var/run/docker.sock', '/var/run/docker.sock')
      expect(docker_command).to have_received(:volume)
        .with('/tmp/gitlab-qa/gitlab-qa-run-2018-07-11-10-00-00-abc123/gitlab-ce-qa-def456', File.join(Gitlab::QA::Docker::Volumes::QA_CONTAINER_WORKDIR, 'tmp'))
    end

    context 'when preparing QA image' do
      let(:docker_engine) { spy('docker engine') }
      let(:qa_image) { 'gitlab/gitlab-ce-qa' }
      let(:qa_tag) { 'latest' }
      let(:release) { double('release', project_name: 'gitlab-ce', qa_image: qa_image, qa_tag: qa_tag, login_params: nil) }

      before do
        stub_const('Gitlab::QA::Docker::Engine', docker_engine)
      end

      context 'when not skipping Docker pulls' do
        it 'pulls the QA image' do
          described_class.perform do |specs|
            specs.suite = suite
            specs.release = release
          end

          expect(docker_engine).to have_received(:pull)
            .with(image: "#{qa_image}:#{qa_tag}")
        end
      end

      context 'when skipping Docker pulls' do
        it 'does not pull QA image' do
          allow(Gitlab::QA::Runtime::Env)
            .to receive(:skip_pull?)
            .and_return(true)

          described_class.perform do |specs|
            specs.suite = suite
            specs.release = release
          end

          expect(docker_engine).not_to have_received(:pull)
        end
      end

      context 'when Runtime::Scenario.qa_image is set' do
        let(:custom_qa_image) { 'custom-qa-image:v1' }

        before do
          stub_const('Gitlab::QA::Runtime::Scenario', spy)
          allow(Gitlab::QA::Runtime::Scenario.attributes).to receive(:include?).and_return(true)
          allow(Gitlab::QA::Runtime::Scenario).to receive(:qa_image).and_return(custom_qa_image)
        end

        it 'pulls the custom QA image' do
          described_class.perform do |specs|
            specs.suite = suite
            specs.release = release
          end

          expect(docker_engine).to have_received(:pull)
            .with(image: custom_qa_image)
        end
      end
    end

    describe 'Docker::Engine#run arguments' do
      let(:docker_engine) { spy('docker engine') }

      before do
        stub_const('Gitlab::QA::Docker::Engine', docker_engine)
      end

      it 'accepts a GitLab image' do
        described_class.perform do |specs|
          specs.suite = suite
          specs.release = Gitlab::QA::Release.new('gitlab/gitlab-ce:foobar')
        end

        expect(docker_engine).to have_received(:run)
          .with(image: 'gitlab/gitlab-ce-qa:foobar', args: [suite])
      end

      it 'accepts a GitLab QA image' do
        described_class.perform do |specs|
          specs.suite = suite
          specs.release = Gitlab::QA::Release.new('gitlab/gitlab-ce-qa:foobar')
        end

        expect(docker_engine).to have_received(:run)
          .with(image: 'gitlab/gitlab-ce-qa:foobar', args: [suite])
      end

      context 'when Runtime::Scenario.qa_image is set' do
        let(:custom_qa_image) { 'custom-qa-image:v1' }

        before do
          stub_const('Gitlab::QA::Runtime::Scenario', spy)
          allow(Gitlab::QA::Runtime::Scenario.attributes).to receive(:include?).and_return(true)
          allow(Gitlab::QA::Runtime::Scenario).to receive(:qa_image).and_return(custom_qa_image)
        end

        it 'runs the custom QA image' do
          described_class.perform do |specs|
            specs.suite = suite
            specs.release = Gitlab::QA::Release.new('ce')
          end

          expect(docker_engine).to have_received(:run)
            .with(image: custom_qa_image, args: [suite])
        end
      end

      context 'when release.login_params are present' do
        it 'log into the container registry' do
          release = double('release', project_name: 'gitlab-ce', qa_image: 'gitlab/gitlab-ce-qa', qa_tag: 'latest', login_params: { foo: :bar })
          described_class.perform do |specs|
            specs.suite = suite
            specs.release = release
          end

          expect(docker_engine).to have_received(:login).with(release.login_params)
          expect(docker_engine).to have_received(:run)
            .with(image: 'gitlab/gitlab-ce-qa:latest', args: [suite])
        end
      end

      context 'when --no-tests is passed' do
        let(:scenario) { spy('scenario') }

        before do
          stub_const('Gitlab::QA::Runtime::Scenario', scenario)

          allow(scenario).to receive(:run_tests).and_return(false)
        end

        it 'skips tests' do
          described_class.perform do |specs|
            specs.suite = suite
            specs.release = spy('release')
          end

          expect(docker_engine).not_to have_received(:run)
        end
      end

      context 'when --enable-feature is passed in args' do
        let(:args) { %w[http://abc.test --enable-feature a] }

        it 'mentions the feature flag and runs exactly once' do
          expect do
            described_class.perform do |specs|
              specs.suite = suite
              specs.args = args
              specs.release = Gitlab::QA::Release.new('EE')
            end
          end.to output(/Running with feature flag/).to_stdout

          expect(docker_engine).to have_received(:run).once
        end
      end

      context 'when --enable-feature and --disable-feature are passed in args' do
        let(:args) { %w[http://abc.test --disable-feature a --enable-feature b] }

        it 'runs twice, once for each feature flag' do
          described_class.perform do |specs|
            specs.suite = suite
            specs.args = args
            specs.release = Gitlab::QA::Release.new('EE')
          end

          expect(docker_engine).to have_received(:run)
            .with(image: 'gitlab/gitlab-ee-qa:nightly', args: [suite, 'http://abc.test', '--disable-feature', 'a'])
          expect(docker_engine).to have_received(:run)
            .with(image: 'gitlab/gitlab-ee-qa:nightly', args: [suite, 'http://abc.test', '--enable-feature', 'b'])
        end
      end

      context 'when rspec args are specified and no feature flags passed' do
        let(:args) { %w[http://abc.test -- file/path --tag focus] }

        it 'passes the args' do
          described_class.perform do |specs|
            specs.suite = suite
            specs.args = args
            specs.release = Gitlab::QA::Release.new('EE')
          end
          expect(docker_engine).to have_received(:run)
            .with(image: 'gitlab/gitlab-ee-qa:nightly', args: [suite, 'http://abc.test', '--', 'file/path', '--tag', 'focus'])
        end

        it 'does not mention feature flags' do
          expect do
            described_class.perform do |specs|
              specs.suite = suite
              specs.release = Gitlab::QA::Release.new('EE')
              specs.args = args
            end
          end.not_to output(/Running with feature flag/).to_stdout
        end
      end
    end
  end
end
