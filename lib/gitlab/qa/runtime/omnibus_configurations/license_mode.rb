# frozen_string_literal: true

module Gitlab
  module QA
    module Runtime
      module OmnibusConfigurations
        class LicenseMode < Default
          def configuration
            <<~OMNIBUS
              gitlab_rails['env'] = { 'GITLAB_LICENSE_MODE' => 'test' }
            OMNIBUS
          end
        end
      end
    end
  end
end
